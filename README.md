# meta-sah-tools

Tools layer for building sah components and images.

## Adding the meta-sah-tools layer to your build

```bash
bitbake-layers add-layer meta-sah-tools
```

## Layer content


### Classes


#### sah_base
This class is the basis needed to build sah components. It implements the tasks `do_configure`, `do_compile` and `do_install`.

- do_configure:
  - The config options defined in the variables `SAH_CONFIG` and `GLOBAL_SAH_CONFIG` are used to create the files `components.config`, `include/components.h`, `components.config.m4` and `components.config_m4.m4` in the `CONFIGDIR=${WORKDIR}/config`. The config options from dependencies are also included in these files. This is done by creating a `${PN}.cfg` file in `/usr/include/sah_config` and including this file in the `${PN}-dev` package. This way a dependant component can access this file and read out its options.
  - **Note:** The idea is that over time the number of config options is greatly reduced and that it won't be necessary anymore to include the config options from the dependencies.

- do_compile:
  - checks if `${S}/${SAH_MAKEFILE_NAME}` exists and calls the compile target. `${S}` is the sources dir and `SAH_MAKEFILE_NAME=Makefile` Extra variables passed to the makefile:

```bash
EXTRA_OEMAKE += "CONFIGDIR="${WORKDIR}/config" INSTALL="install" D=${D} S=${S} PV=${PV} PKG_CONFIG_LIBDIR=${D}/usr/lib/pkgconfig STAGINGDIR=${CONFIGDIR}"
```

- do_install:
  - checks if `${S}/${SAH_MAKEFILE_NAME}` exists and calls the compile target. `${S}` is the sources dir and `SAH_MAKEFILE_NAME=Makefile` Extra variables passed to the makefile:

```bash
EXTRA_OEMAKE += "CONFIGDIR="${WORKDIR}/config" INSTALL="install" D=${D} S=${S} PV=${PV} PKG_CONFIG_LIBDIR=${D}/usr/lib/pkgconfig STAGINGDIR=${CONFIGDIR}"
```

The class also adds the files `/usr/lib/*/lib*.so.*` and `/usr/lib/*/*.odl` to the package ${PN} and the files `/usr/lib/*/lib*.so` are to the package `${PN}-dev`

#### sah_git
Sets the variables `SRC_URI`, `S`, `SRCREV` and `PV`.

- `SRC_URI`: The source uri `SRC_URI` is set to `${@sah_srcuri(d)}`. The function will read out the variables `GIT_BASE_URL`, `SAH_GIT_PATH_${PN}` and `SAH_GIT_REF_${PN}` and construct the correct `SRC_URI`.
- `S`: The source directory `S` is set to `${@sah_srcdir(d)}`. The function returns `${WORKDIR}/git`.
- `SRCREV`: The source revision `SRCREV` is set to `${@sah_srcrev(d)}`. The function will read out the variable `SAH_GIT_REF_${PN}`. If this variable is not set, it will return `master` If this variable is a ref, the function will return an empty string. If this variable is a branch it wil return `${AUTOREV}`.
- `PV`: The package version `PV` is set to `${@sah_pv(d)}`. This function will set the package version to the git reference name if a tag is being used. If a branch is being used, the package version is set to `<BRANCH_NAME>+<COMMIT_ID>`

#### sah_initscripts
This class replaces the `install_init_release` and `install_init functions` defined in `sahcommon.mk` in the legacy build system.
In the `Makefile`, the file should be installed `${D}/etc/init.d/<NAME>`.
In the recipe, the class should be inherited and the variable `INITSCRIPT_PARAM="<NAME>:<START>:<STOP>"` should be set.

The class will then create a symbolic links between:
* `/etc/init.d/<NAME>` and `/etc/rc1.d/S<START><NAME>:`
* `/etc/init.d/<NAME>` and `/etc/rc6.d/K<STOP><NAME>`

If `<STOP>` is left empty, no stop script will be created. By seperating multiple instances with a space in the `INITSCRIPT_PARAM` variable, it is possible to add multiple initscripts.


#### sah_repo
This class inherits the "externalsrc" class and should make it easy to use a local repo during development. The idea is that you clone a git repo in `${SAH_SRC_ROOT}/${SAH_GIT_PATH_${PN}}`, where `${SAH_SRC_ROOT}` should be set in your `local.conf` and `${SAH_GIT_PATH_${PN}}` can either be taken from the `meta-componentlst` or can be overwritten in your `local.conf` file. You also need to set the `PREFFERED_VERSION_${PN} = "repo"` in the `local.conf` as this file won't be taken by default. The class also calls the clean script in the `${SAH_MAKEFILE_NAME}`.

**Note:** If you use these variables in your `local.conf`, you need to replace `${PN}` with the hardcoded name of your package.

### Recipes
- openssl: Yocto warrior has recipes for `openssl_1.1.1b` and `openssl10_v1.0.2r`, but it makes it very difficult to switch between them. In Yocto Zeus `openssl 1.0*` isn't included at all. As a quick fix, the `openssl 1.0.2r` is included in this layer. Over time we should move to `v1.1*`.

- sah-initscripts: Basic sah startup initialization scripts for the system. This is a first poc of initscripts in yocto, but is not workable over the long term as it contains files from `packaging_gwrootfs`. `packaging_gwrootfs` needs to be cleaned up first or needs to be used as a source.

