do_install_append(){

    for elem in ${BACKUPRESTORE_PARAM}; do
        name=$(echo ${elem} | cut -d ":" -f1 )
        level=$(echo ${elem} | cut -d ":" -f2 )
        if [ -z ${name} ]; then
            bbwarn "Inherites sah_backuprestore and BACKUPRESTORE_NAME is not defined" 
        else
            if [ -z ${level} ]; then
                bbwarn "Inherites sah_backuprestore and BACKUPRESTORE_LEVEL is not defined"
            fi 
            install -d ${D}/usr/lib/hgwcfg/backup
            install -d ${D}/usr/lib/hgwcfg/restore
            install -d ${D}/usr/lib/hgwcfg/import
            ln -sfr ${D}/etc/init.d/${name} ${D}/usr/lib/hgwcfg/backup/B${level}${name}
            ln -sfr ${D}/etc/init.d/${name} ${D}/usr/lib/hgwcfg/restore/R${level}${name}
            ln -sfr ${D}/etc/init.d/${name} ${D}/usr/lib/hgwcfg/import/R${level}${name}
        fi
    done
}

FILES_${PN} += "/usr/lib/hgwcfg/*/*"