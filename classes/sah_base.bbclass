SAH_MAKEFILE_NAME ??= "Makefile"
CONFIGDIR ??= "${WORKDIR}/config"

python sah_base_do_configure() {
    def isint(var):
        if var.startswith("0") and len(var) != 1:
            return False
        else:
            try:
                int(var)
                return True
            except ValueError:
                return False

    def get_key_value(item):
        key = item.split("=")[0]
        value = d.getVar(key,True)
        if isint(value):
            value=int(value)
        elif value not in ['y','n']:
            value = '\"' + value + '\"'
        return key, value

    def add_option_from_variable(var, options):
        if d.getVar(var,True):
            for item in d.getVar(var,True).split():
                key, value = get_key_value(item)
                if key in options.keys():
                    bb.fatal("key {} was already defined".format(key))
                bb.debug(2,"Writing {}={} of variable {} to config options".format(key, value, var))
                options[key] = value
        return options

    def add_option_from_dependencies(comp,options):
        sysroot_path=d.getVar("STAGING_DIR_HOST",True)
        dep_cfg = os.path.join(sysroot_path,"usr/include/sah_config/{}.cfg".format(comp))
        if os.path.isfile(dep_cfg):
            bb.debug(1,"Reading {}".format(dep_cfg))
            with open(dep_cfg,'r') as file:
                for line in file:
                    key = line.split("=")[0]
                    value = line.split("=")[1].strip("\n")
                    if key in options.keys():
                        bb.fatal("key {} was already defined".format(key))
                    bb.debug(2,"Writing {}={} of {} dependencyto config options".format(key, value, comp))
                    options[key] = value
        else:
            bb.debug(1,"Skipping {}. {} file not found".format(comp,dep_cfg))
        return options

    def read_out_options():
        comp_options = {}
        comp_options = add_option_from_variable("SAH_CONFIG",comp_options)
        all_options = comp_options.copy()
        all_options = add_option_from_variable("GLOBAL_SAH_CONFIG", all_options)
        for comp in d.getVar("DEPENDS", True).split():
            all_options = add_option_from_dependencies(comp,all_options)
        return comp_options, all_options

    def create_config_files(filepath,config_options):
        if os.path.exists(filepath) and not os.path.isfile(filepath):
            bb.error("Error, %s is not a file" % fp)
            raise IOError
        with open(filepath,'w') as file:
            for key,value in config_options.items():
                if value is not 'n':
                    if filepath.endswith(".h"):
                        if value is 'y':
                            value = 1
                        file.write('#define {} {}\n'.format(key.replace("-","_"), value))
                    elif filepath.endswith("_m4.m4"):
                        m4_value = value
                        if not isinstance(value, int):
                            m4_value = "{}".format(value.replace("\"",""))
                        file.write('m4_define(`{}\',`{}\')m4_dnl\n'.format(key.replace("-","_"), m4_value))
                    elif filepath.endswith(".m4"):
                        m4_value = value
                        if not isinstance(value, int):
                            m4_value = "{}".format(value.replace("\"",""))
                        file.write('define(`{}\',`{}\')dnl\n'.format(key.replace("-","_"), m4_value))
                    elif filepath.endswith(".config"):
                        file.write('{}={}\n'.format(key,value))
                    elif filepath.endswith(".cfg"):
                        file.write('{}={}\n'.format(key,value))
                    else:
                        raise NotImplementedError

    configdir = d.getVar("CONFIGDIR",True)
    pn = d.getVar("PN",True)
    comp_options, all_options = read_out_options()
    config_files = ["components.config","components.h","components.config.m4","components.config_m4.m4", "{}.cfg".format(pn)]
    for filename in config_files:
        options = all_options
        filepath = os.path.join(configdir,filename)
        if filename.endswith(".h"):
            filepath = os.path.join("{}/include".format(configdir),filename)
        elif filename.endswith(".cfg"):
            options = comp_options
        if options:
            bb.debug(1,"Writing to {}".format(filepath))
    
            create_config_files(filepath,options)
        else:
            bb.debug(1, "No options found")
}

do_configure[dirs] += "${CONFIGDIR} ${CONFIGDIR}/include"
do_configure[sstate-ouputdirs] += "${CONFIGDIR}"
do_configure[sstate-inputdirs] += "${CONFIGDIR}"

def read_out_sah_config(d):
    out = " "
    options = d.getVar("SAH_CONFIG", True)
    if options:
        out += options
    options = d.getVar("GLOBAL_SAH_CONFIG", True)
    if options:
        out += options
    return out

do_configure[vardeps] += "${@read_out_sah_config(d)} "


sah_base_do_compile(){
    if [ -e ${S}/${SAH_MAKEFILE_NAME} ]; then
        oe_runmake -f ${SAH_MAKEFILE_NAME} compile || die "make failed"
    else
        bbnote "nothing to compile"
    fi
}

sah_base_do_install () {
    if [ -e ${S}/${SAH_MAKEFILE_NAME} ]; then
        oe_runmake -f ${SAH_MAKEFILE_NAME} install || die "make failed"
    else
        bbnote "nothing to install"
    fi
    if [ -f "${CONFIGDIR}/${PN}.cfg" ]; then 
        install -D -m 0644 ${CONFIGDIR}/${PN}.cfg ${D}/usr/include/sah_config/${PN}.cfg
        bbdebug 1 "install ${PN}.cfg"
    else
        bbdebug 1 "${CONFIGDIR}/${PN}.cfg not found"
    fi
}

EXPORT_FUNCTIONS do_configure do_compile do_install

EXTRA_OEMAKE += "CONFIGDIR="${CONFIGDIR}" \
                 INSTALL="install" \
                 D=${D} S=${S} PV=${PV} \
                 PKG_CONFIG_LIBDIR=${D}/usr/lib/pkgconfig \
                 STAGINGDIR=${CONFIGDIR} \
                 HARDCO_HAL_DIR=${STAGING_DIR_HOST}/usr/include"

FILES_${PN} += "/usr/lib/*/*.odl /usr/lib/debuginfo/D*"
FILES_${PN} += "/usr/lib/*/lib*${SOLIBS}"
FILES_${PN}-dev += "/usr/lib/*/lib*${SOLIBSDEV}"