GIT_BASE_URL ?= "gitlab.com/soft.at.home"

def find_sah_git_ref(d):
    pn = d.getVar("PN",True)
    sah_git_ref = d.getVar('SAH_GIT_REF_{}'.format(pn),True)
    if not sah_git_ref:
        sah_git_ref="master"
    return sah_git_ref

def sah_srcuri(d):
    git_base_url = d.getVar("GIT_BASE_URL",True)
    pn = d.getVar("PN",True)
    sah_git_path = d.getVar('SAH_GIT_PATH_{}'.format(pn), True)
    sah_git_ref = find_sah_git_ref(d)
    if sah_git_ref.startswith("refs/tags/"):
        sah_git_ref = sah_git_ref.split("/")[2]
        srcuri = "git://git@{}/{}.git;nobranch=1;tag={};protocol=ssh".format(git_base_url,sah_git_path,sah_git_ref)
    else:
        srcuri = "git://git@{}/{}.git;branch={};protocol=ssh".format(git_base_url,sah_git_path,sah_git_ref)
    bb.debug(1, "{} added to SRC_URI".format(srcuri))
    return srcuri

def sah_srcdir(d):
    return '${WORKDIR}/git'

def sah_pv(d):
    sah_git_ref = find_sah_git_ref(d)
    if sah_git_ref.startswith("refs/tags/"):
        sah_git_ref = sah_git_ref.split("/")[2]
        return sah_git_ref
    else:
        return "{}+${}".format(sah_git_ref,"{SRCPV}")      

def sah_srcrev(d):
    sah_git_ref = find_sah_git_ref(d)
    if sah_git_ref.startswith("refs/tags/"):
        return ""
    else:
        return "${}".format("{AUTOREV}")

SRC_URI += "${@sah_srcuri(d)}"
S = "${@sah_srcdir(d)}"
SRCREV = "${@sah_srcrev(d)}"
PV = "${@sah_pv(d)}"
