do_install_append(){
    if [ -z ${INITSCRIPT_PARAM} ]; then
        bbfatal "Inherited initscripts_sah but INITSCRIPT_PARAM is not defined"
    fi
    for elem in ${INITSCRIPT_PARAM}; do
        name=$(echo ${elem} | cut -d ":" -f1 )
        start=$(echo ${elem} | cut -d ":" -f2 )
        stop=$(echo ${elem} | cut -d ":" -f3 )
        install -d ${D}/etc/rc.d
        if [ -n "${start}" ]; then
            install -d ${D}/etc/rc3.d
            bbdebug 2 "Creating startup link for ${name} with level ${start}"
            ln -sfr ${D}/etc/init.d/${name} ${D}/etc/rc3.d/S${start}${name}
            ln -sfr ${D}/etc/init.d/${name} ${D}/etc/rc.d/S${start}${name}
        else
            bbfatal "Inherited initscripts_sah but start is not defined"
        fi
        if [ -n "${stop}" ]; then
            install -d ${D}/etc/rc6.d
            bbdebug 2 "Creating stop link for ${name} with level ${stop}"
            ln -sfr ${D}/etc/init.d/${name} ${D}/etc/rc6.d/K${stop}${name}
            ln -sfr ${D}/etc/init.d/${name} ${D}/etc/rc.d/K${stop}${name}
        fi
    done
}

