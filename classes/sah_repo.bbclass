inherit externalsrc
EXTERNALSRC = "${SAH_SRC_ROOT}/${SAH_GIT_PATH_${PN}}"
EXTERNALSRC_BUILD = "${SAH_SRC_ROOT}/${SAH_GIT_PATH_${PN}}"

#Deprioritize recipe, so it is not chosen by bitbake unless explicitely asked with PREFERRED_VERSION
DEFAULT_PREFERENCE = "-1"

sah_repo_do_buildclean(){
    if [ ! -z ${SAH_MAKEFILE_NAME} ] && [ -e ${EXTERNALSRC}/${SAH_MAKEFILE_NAME} ]; then
        oe_runmake -f ${EXTERNALSRC}/${SAH_MAKEFILE_NAME} clean || die "make failed"
    else
        bbnote "nothing to do - no makefile found"
    fi
}

addtask do_buildclean before do_clean
EXPORT_FUNCTIONS do_buildclean
do_clean[deptask]="do_buildclean"
